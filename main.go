package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/building-service/proto/building"
)

func main() {
	srv := micro.NewService(

		micro.Name("building-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewBuildingServiceClient("building-service", client.DefaultClient)
	// Get building information
	{
		id := uniuri.New()

		start := time.Now()

		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		resp, err := c.GetBuildingInfo(ctx, &pb.Building{})

		if err != nil {
			log.Panicf("Error: Did not get any building information - %v", err)
		}

		log.Printf("Building Info: %v", resp.Building)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}

	// Get all floors
	{
		id := uniuri.New()

		start := time.Now()

		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		resp, err := c.GetAllFloors(ctx, &pb.Floor{})

		if err != nil {
			log.Panicf("Error: Did not get all the floors - %v", err)
		}

		log.Printf("Floors: %v", resp.Floors)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}

	// Get specific floor
	{
		id := uniuri.New()

		start := time.Now()

		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		floorID := "5c72eaec1c9d4400006ed3c8"
		resp, err := c.GetFloor(ctx, &pb.Floor{Id: floorID})

		if err != nil {
			log.Panicf("Error: Did not get floor with ID: %s - %v", floorID, err)
		}

		log.Printf("Floor: %v", resp.Floor)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
