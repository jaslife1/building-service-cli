build:
	docker build -t building_service_cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		building_service_cli